import React from 'react'
import ROUTES from '../routes'

import customersService from './customers-service'

import CustomerForm from './customer-form'

class CustomerNew extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      customer: {}
    }
  }

  create() {
    customersService.createCustomer(this.state.customer).then(res => {
      this.props.history.push(ROUTES.CUSTOMER_LISTING)
    })
  }

  handleChange(e) {
    this.setState({
      ...this.state,
      customer: {
        ...this.state.customer,
        [e.target.name]: e.target.value,
      }
    })
  }

  render() {
    const customer = this.state.customer

    return (
      <div className="box">
        <div className="box-header">
          <h3 className="box-title">New Customer</h3>
        </div>
        <div className="box-body">
          <CustomerForm customer={customer} handleChange={e => this.handleChange(e)} />
        </div>
        <div className="box-footer">
          <button className="btn btn-primary" onClick={() => this.create()}>Create</button>
        </div>
      </div>
    )
  }
}

export default CustomerNew
