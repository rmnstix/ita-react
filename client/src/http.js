import axios from 'axios'

const port = window.location.hostname.includes("heroku") ? "" : ":8081"

export default axios.create({baseURL: window.location.protocol + '//' + window.location.hostname + port, withCredentials: true})
